import Vue from 'vue'
import App from './App.vue'
import router from './router'

import { library } from '@fortawesome/fontawesome-svg-core';
import { faEnvelopeSquare, faPhoneSquare, faExternalLinkAlt, faCode, faDraftingCompass, faMars  } from '@fortawesome/free-solid-svg-icons';
library.add(faEnvelopeSquare, faPhoneSquare, faExternalLinkAlt, faCode, faDraftingCompass, faMars);
// 全局组件
import Tag from './components/Tag.vue';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';

Vue.config.productionTip = false;
//注册全局组件
Vue.component('Tag', Tag);
Vue.component('font-awesome-icon', FontAwesomeIcon)

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
